"""
Thie Program is to get value of Pi till 'n' number of decimal places
We are using Newton-Raphson convergence and Chudnovsky's seires algorithms

"""
import math


def sqrt(n, one):
    """
    Return the Square Root of 'n', with precsion passed in as one.
    It uses Second-Order Newton-Raphson convergence.

    """
    # Floating point arithmetic to make an initial guess
    floating_point__precision = 10 ** 16
    n_float = float((n * floating_point__precision) // one) / floating_point__precision
    x = (int(floating_point__precision * math.sqrt(n_float)) * one) // floating_point__precision
    n_one = n * one
    while 1:
        x_old = x
        x = (x + n_one // x) // 2
        if x == x_old:
            break
    return x



def pi_chudnovksy(one):
    """
    Calculate pi using Chudnovsky's series
    This calcualtes it in fixed point, using the value for one passed in.

    """
    k = 1
    a_k = one
    a_sum = one
    b_sum = 0
    C = 640320
    C3_OVER_24 = C**3 // 24
    while 1:
        a_k *= -(6*k-5)*(2*k-1)*(6*k-1)
        a_k //= k**3*C3_OVER_24
        a_sum += a_k
        b_sum += k * a_k
        k += 1
        if a_k == 0:
            break
    total = 13591409*a_sum + 545140134*b_sum
    pi = (426880*sqrt(10005*one, one)*one) // total
    return pi



def validate_non_negative_integer():
    """
    Ask for User Input and less than 10000.
    """
    while True:

        s = input("How many digits do you want to see?\n")

        try:
            digits = int(s)
            if digits >= 10000:
                print("Enter a number smaller than 1000.0")
            elif digits > 0:
                return digits
            else:
                print("Enter a non-negative integer.")
        except ValueError:
            print("Enter a Valid Integer.")



def main():
    digits = validate_non_negative_integer()
    pi = str(pi_chudnovksy(10**(digits * 10)))[:digits]
    print(pi[0] + "." + pi[1:])


if __name__ =="__main__":
    main() # runs main() which is defined above
