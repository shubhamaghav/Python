"""
This Program is to get Armstrong number in a give range.
An Armstrong number of three digits is an integer such that the sum of the cubes of its digits is equal to the number itself.
"""

#take input from the User
lower = int(input("Enter Lower range:"))
upper = int(input("Enter Upper range:"))
armstrong=[]
for num in range(lower,upper + 1):
    #initialise sum
    sum = 0

    #find the sum of the cube of each digit
    temp = num
    while temp>0:
        digit = temp % 10 # Modulus:Return remainder
        sum += digit ** 3
        temp //= 10 # Floor Division: Return Quoitent

    if num == sum:
        armstrong.append(num)

print(armstrong)
