"""
This Program is to check whether given number is armstrong number or not.
An Armstrong number of three digits is an integer such that the sum of the cubes of its digits is equal to the number itself
"""

#take input from the User
num = int(input("Enter a Number:"))

#initialise sum
sum = 0

#find the sum of the cube of each digit
temp = num
while temp>0:
    digit = temp % 10 # Modulus:Return remainder
    sum += digit ** 3
    temp //= 10 # Floor division: Return Quoitent

#display result
if num == sum:
    print("{} is an Armstrong Number ".format(num))
else:
    print("{} is not an Armstrong Number ".format(num))
